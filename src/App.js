import React from "react";

import "bootstrap/dist/js/bootstrap.min.js";

import Home from "./screens/Home";
import About from "./screens/About";
import Gallery from "./screens/Gallery";
import Services from "./screens/Services";
import Admin from "./screens/Admin";

import Booking from "./screens/Booking";
import Register from "./screens/Register";
import Login from "./screens/Login";

import AuthState from "./context/auth/AuthState";

import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <>
      <AuthState>
        <Router>
          <Navbar />
          <Switch>
            <Route path="/" exact component={Home} />

            <Route path="/about" exact component={About} />

            <Route path="/gallery" exact component={Gallery} />

            <Route path="/services" exact component={Services} />

            <Route path="/admin" exact component={Admin} />

            <Route path="/booking" exact component={Booking} />

            <Route path="/register" exact component={Register} />

            <Route path="/login" exact component={Login} />
          </Switch>
        </Router>
      </AuthState>
    </>
  );
}

export default App;
