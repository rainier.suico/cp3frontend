import React, { Fragment, useContext } from "react";
import useScript from "../hooks/useScript";
import { Link } from "react-router-dom";
import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import SweetAlert from "sweetalert2-react";
import {
  FormGroup,
  Form,
  Label,
  Input,
  Container,
  UncontrolledTooltip,
  Button,
  Row,
  Col,
  InputGroup
} from "reactstrap";

import DateTime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import moment from "moment";
import AuthContext from "../context/auth/authContext";
const Book = props => {
  useScript("./js/jquery.min.js");
  useScript("./js/popper.min.js");
  useScript("./js/bootstrap.min.js");
  useScript("./js/alime.bundle.js");
  useScript("./js/default-assets/active.js");

  const [books, setBooks] = React.useState([]);

  const authContext = useContext(AuthContext);
  const { isAuthenticated } = authContext;
  const [success, setSuccess] = React.useState(false);
  const [deleteSuccess, setDeleteSuccess] = React.useState(false);
  const onSubmit = e => {
    e.preventDefault();

    if (!isAuthenticated) {
      props.history.push("/register");
    }

    const book = {
      date_time: date,
      name: name
    };

    if (localStorage.jwtToken) {
      const token = localStorage.jwtToken;
      setAuthToken(token);
    }

    axios
      .post("/api/bookings/book", book)
      .then(res => {
        setName("");
        setSuccess(true);
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });

    axios
      .get("/api/bookings/mybook")
      .then(res => setBooks(res.data)) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };

  React.useEffect(() => {
    if (localStorage.jwtToken) {
      const token = localStorage.jwtToken;
      setAuthToken(token);
    }

    axios
      .get("/api/bookings/mybook")
      .then(res => setBooks(res.data)) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  }, []);

  const [name, setName] = React.useState("");
  const [date, setDate] = React.useState(new Date());

  const onChange = e => {
    setName(e.target.value);
  };

  const onDateChange = d => {
    console.log(d);
    setDate(d);
  };

  const showAlert = () => {
    return (
      <SweetAlert
        show={success}
        title="Booking Successful"
        text="Wait for confirmation"
        onConfirm={() => setSuccess(false)}
      />
    );
  };

  const deleteBooking = id => {
    if (localStorage.jwtToken) {
      const token = localStorage.jwtToken;
      setAuthToken(token);
    }

    axios
      .delete("/api/bookings/" + id)
      .then(res => {
        axios
          .get("/api/bookings/mybook")
          .then(res => {
            setBooks(res.data);
          }) // re-direct to login on successful register
          .catch(err => {
            console.log(err.message);
          });
      }) // re-direct to login on successful register
      .catch(err => {
        console.log(err.message);
      });
  };
  return (
    <>
      <section
        className="breadcrumb-area bg-img bg-overlay jarallax"
        style={{ backgroundImage: `url(img/bg-img/65.jpeg)` }}
      >
        <div className="container h-100">
          <div className="row h-100 align-items-center">
            <div className="col-12">
              <div className="breadcrumb-content text-center">
                <h2 className="page-title">Book Now</h2>
                <nav aria-label="breadcrumb">
                  <ol className="breadcrumb justify-content-center">
                    <li className="breadcrumb-item">
                      <Link to="/">
                        <i className="icon_house_alt"></i> Home
                      </Link>
                    </li>
                    <li className="breadcrumb-item active" aria-current="page">
                      Book
                    </li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="contact-area section-padding-80-50">
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-6">
              <h2 className="contact-title mb-30">
                Let Us, <br />
                Shoot You
              </h2>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="contact-info mb-30">
                <p>Email</p>
                <a href="#">myriad.x.69@gmail.com</a>
              </div>

              <div className="contact-info mb-30">
                <p>Contact #</p>
                <a href="#">(02) 8-884-8888</a>
              </div>
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div className="contact-info mb-30">
                <p>Address</p>
                <a href="#">Pegasus, Diliman, Quezon City, 1113</a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="map-area section-padding-0-80">
        <div className="container">
          <div id="map">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15441.778337173699!2d121.0165137!3d14.6306865!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe87c16344bdaf8bb!2sPegasus!5e0!3m2!1sfil!2sph!4v1576391204359!5m2!1sfil!2sph"
              width="600"
              height="450"
              frameborder="0"
              style={{ border: "0" }}
              allowfullscreen=""
            ></iframe>
          </div>
        </div>
      </div>

      <Container style={{ background: "white" }}>
        <div className="text-center">
          <Col md="12">
            <div className="title mb-3">
              <h3>Booking Schedule</h3>
            </div>
            <Row>
              
              <Col sm="3">
                {success && showAlert()}
                <Form onSubmit={onSubmit}>
                  <FormGroup>
                    <Label>Date/Time</Label>
                    <InputGroup>
                      <DateTime onChange={onDateChange} />
                    </InputGroup>
                    <Label>Name</Label>
                    <Input
                      onChange={onChange}
                      value={name}
                      type="text"
                      name="name"
                      id="name"
                      placeholder="Name"
                    />
                  </FormGroup>
                  <Button className="btn btn-primary">Book</Button>
                </Form>
              </Col>
             

              <Col sm="9">
                <table className="table table-dark">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Date/Time</th>
                      <th scope="col">Name</th>
                      <th scope="col">Status</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {books.map((item, index) => (
                      <tr key={index}>
                        <th scope="row">{++index}</th>
                        <td>
                          {moment(item.date_time).format("MM/DD/YYYY HH:mm A")}
                        </td>
                        <td>{item.name}</td>
                        <td>{item.status}</td>
                        <td>
                          <Button
                            className=" fa fa-trash btn-round mr-1"
                            color="danger"
                            id="tooltip392938669"
                            outline
                            onClick={() => deleteBooking(item._id)}
                          ></Button>
                          <UncontrolledTooltip
                            delay={0}
                            placement="left"
                            target="tooltip392938669"
                          >
                            Delete
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </Col>
            </Row>
          </Col>
        </div>
      </Container>

      <div className="follow-area clearfix mt-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="section-heading text-center">
                <h2>Follow Instagram</h2>
                <p>@myriad_69</p>
              </div>
            </div>
          </div>
        </div>

        <div className="instragram-feed-area owl-carousel">
          <div className="single-instagram-item">
            <img src="img/bg-img/11.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/12.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/13.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/14.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/15.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/16.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <footer className="footer-area">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="footer-content d-flex align-items-center justify-content-between">
                <div className="copywrite-text">
                  <p>
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> |
                    Myriad
                  </p>
                </div>

                <div className="social-info">
                  <a href="https://www.facebook.com">
                    <i className="ti-facebook" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.twitter.com">
                    <i className="ti-twitter-alt" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.linkedin.com">
                    <i className="ti-linkedin" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.pinterest.com">
                    <i className="ti-pinterest" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Book;
