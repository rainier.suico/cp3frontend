import React, { Fragment } from "react";
import useScript from "../hooks/useScript";
import { Link } from "react-router-dom";

const Home = props => {
  // useScript("./js/bootstrap.min.js");
  useScript("./js/jquery.min.js");
  useScript("./js/wow.min.js");

  useScript("./js/alime.bundle.js");
  useScript("./js/default-assets/active.js");

  // useScript("./js/popper.min.js");
  // useScript("./js/output.min.js");

  return (
    <Fragment>
      <section className="welcome-area">
        <div className="welcome-slides owl-carousel">
          <div
            className="single-welcome-slide bg-img bg-overlay"
            style={{ backgroundImage: `url(img/bg-img/3.jpg)` }}
          >
            <div className="container h-100">
              <div className="row h-100 align-items-center">
                <div className="col-12 col-lg-8 col-xl-6">
                  <div className="welcome-text">
                    <h2 data-animation="bounceInDown" data-delay="900ms">
                      Hello there!
                    </h2>
                    <p data-animation="bounceInDown" data-delay="500ms">
                      We are a team of professional photographers with over 10
                      years experience. We have handled various accounts in the
                      past including weddings and other interests.{" "}
                    </p>
                    <div
                      className="hero-btn-group"
                      data-animation="bounceInDown"
                      data-delay="100ms"
                    >
                      <a
                        href="/booking"
                        className="btn alime-btn mb-3 mb-sm-0 mr-4"
                      >
                        Book Now
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="single-welcome-slide bg-img bg-overlay"
            style={{ backgroundImage: `url(img/bg-img/1.jpg)` }}
          >
            <div className="container h-100">
              <div className="row h-100 align-items-center">
                <div className="col-12 col-lg-8 col-xl-6">
                  <div className="welcome-text">
                    <h2 data-animation="bounceInDown" data-delay="900ms">
                      Myriad
                    </h2>
                    <p data-animation="bounceInDown" data-delay="500ms">
                      Feel free to explore our site and experience photographic
                      excellence. Please don't hesitate to book us now.{" "}
                    </p>
                    <div
                      className="hero-btn-group"
                      data-animation="bounceInDown"
                      data-delay="100ms"
                    >
                      <a
                        href="/booking"
                        className="btn alime-btn mb-3 mb-sm-0 mr-4"
                      >
                        Book Now
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="alime-portfolio-area section-padding-80 clearfix">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12">
              <div className="alime-projects-menu">
                <div className="portfolio-menu text-center">
                  <button className="btn active" data-filter="*">
                    All
                  </button>
                  <button className="btn" data-filter=".wedding">
                    Wedding
                  </button>
                  <button className="btn" data-filter=".sexy">
                    Sexy
                  </button>
                  <button className="btn" data-filter=".portrait">
                    Portrait
                  </button>
                  <button className="btn" data-filter=".nature">
                    Nature
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="row alime-portfolio">
            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp"
              data-wow-delay="100ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/3.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/3.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/4.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/4.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item sexy mb-30 wow fadeInUp"
              data-wow-delay="500ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/92.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/92.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp"
              data-wow-delay="700ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/1.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/1.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item nature mb-30 wow fadeInUp"
              data-wow-delay="100ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/107.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/107.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item  wedding mb-30 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/68.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/68.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item sexy mb-30 wow fadeInUp"
              data-wow-delay="500ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/72.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/72.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item wedding mb-30 wow fadeInUp"
              data-wow-delay="700ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/69.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/69.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>

            <div
              className="col-12 col-sm-6 col-lg-3 single_gallery_item nature mb-30 wow fadeInUp"
              data-wow-delay="300ms"
            >
              <div className="single-portfolio-content">
                <img src="img/bg-img/105.jpg" />
                <div className="hover-content">
                  <a href="img/bg-img/105.jpg" className="portfolio-img">
                    +
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div
              className="col-12 text-center wow fadeInUp"
              data-wow-delay="700ms"
            >
              <Link to="/gallery" className="btn alime-btn btn-2 mt-15">
                View More
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div className="follow-area clearfix">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="section-heading text-center">
                <h2>Follow Instagram</h2>
                <p>@myriad_69</p>
              </div>
            </div>
          </div>
        </div>

        <div className="instragram-feed-area owl-carousel">
          <div className="single-instagram-item">
            <img src="img/bg-img/11.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/12.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/13.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/14.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/15.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>

          <div className="single-instagram-item">
            <img src="img/bg-img/16.jpg" />
            <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
              <a href="#">
                <i className="ti-instagram" aria-hidden="true"></i>
                <span>myriad_69</span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <footer className="footer-area">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="footer-content d-flex align-items-center justify-content-between">
                <div className="copywrite-text">
                  <p>
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> |
                    Myriad
                  </p>
                </div>

                <div className="social-info">
                  <a href="https://www.facebook.com">
                    <i className="ti-facebook" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.twitter.com">
                    <i className="ti-twitter-alt" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.linkedin.com">
                    <i className="ti-linkedin" aria-hidden="true"></i>
                  </a>
                  <a href="https://www.pinterest.com">
                    <i className="ti-pinterest" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </Fragment>
  );
};

export default Home;
