import React, {Fragment, useState, useContext, useEffect} from 'react'
import useScript from '../hooks/useScript';
import {Link} from 'react-router-dom';
import AuthContext from '../context/auth/authContext';

const Register = (props) => {

	useScript("./js/jquery.min.js");
	useScript("./js/popper.min.js");
	useScript("./js/bootstrap.min.js");
	useScript("./js/alime.bundle.js");
	useScript("./js/wow.min.js");
	useScript("./js/default-assets/active.js");

	const authContext = useContext(AuthContext);
  	const { registerUser, errors, isAuthenticated } = authContext;

	const [state, setState] = useState({
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    password2: '',
    errors: {}
  	});

  	const onChange = e => {
    setState({ ...state, [e.target.id]: e.target.value });
 	 };

  	const onSubmit = e => {
    e.preventDefault();
    const newUser = {
      username: state.username,
      firstname: state.firstname,
      lastname: state.lastname,
      email: state.email,
      password: state.password,
      password2: state.password2
    };



   
    registerUser(newUser, props.history);
  	};

  	useEffect(() => {
    if (isAuthenticated) {
      props.history.push('/');
    }

    if (errors) {
      setState({ ...state, errors: errors });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  	}, [errors, isAuthenticated, props.history]);

	return (

	<>

	<section className="breadcrumb-area bg-img bg-overlay jarallax" style={{backgroundImage: `url(img/bg-img/register.jpg)`}}>
        <div className="container h-100">
            <div className="row h-100 align-items-center">
                <div className="col-12">
                    <div className="breadcrumb-content text-center">
                        <h2 className="page-title">Register</h2>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb justify-content-center">
                                <li className="breadcrumb-item"><Link to="/"><i className="icon_house_alt"></i> Home</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Register</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div className="container w-50 mt-5">
        <form noValidate onSubmit={onSubmit}>
            <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="username">Username</label>
                  <input  onChange={onChange} value={state.username} type="text" className="form-control" id="username" required/>
                  <span className="text-danger">{state.errors.username}</span>
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="email">Email</label>
                  <input  onChange={onChange}
                          value={state.email} type="email" className="form-control" id="email" required/>
                  <span className="text-danger">{state.errors.email}</span>
                </div>
            </div>

            <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="firstname">First Name</label>
                  <input  onChange={onChange}
                          value={state.firstname} type="text" className="form-control" id="firstname" required/>
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="lastname">Last Name</label>
                  <input  onChange={onChange}
                          value={state.lastname} type="text" className="form-control" id="lastname" required/>
                </div>
            </div>

            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input  onChange={onChange}
                          value={state.password} type="password" className="form-control" id="password" placeholder="Enter Password" required/>
            	<span className="text-danger">{state.errors.password}</span>
            </div>
            
            <div className="form-group">
                <label htmlFor="password2">Confirm Password</label>
                <input  onChange={onChange}
                          value={state.password2} type="password" className="form-control" id="password2" placeholder="Repeat Password" required/>
            	<span className="text-danger">{state.errors.password2}</span>
            </div> 
            
            <button type="submit" className="btn btn-primary btn-block mb-5">Register</button>
        </form>
    </div>

    <div className="follow-area clearfix">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center">
                        <h2>Follow Instagram</h2>
                        <p>@myriad_69</p>
                    </div>
                </div>
            </div>
        </div>

        
        <div className="instragram-feed-area owl-carousel">
            
            <div className="single-instagram-item">
                <img src="img/bg-img/11.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/12.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/13.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/14.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/15.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/16.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    

    
     <footer className="footer-area">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="footer-content d-flex align-items-center justify-content-between">
                        
                        <div className="copywrite-text">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Myriad
</p>
                        </div>
                        
                        
                        
                       <div className="social-info">
                            <a href="https://www.facebook.com"><i className="ti-facebook" aria-hidden="true"></i></a>
                            <a href="https://www.twitter.com"><i className="ti-twitter-alt" aria-hidden="true"></i></a>
                            <a href="https://www.linkedin.com"><i className="ti-linkedin" aria-hidden="true"></i></a>
                            <a href="https://www.pinterest.com"><i className="ti-pinterest" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	</>


	)

}



export default Register;